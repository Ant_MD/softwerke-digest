// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Buefy from 'buefy'
import axios from 'axios'
// import 'buefy/lib/buefy.css'
import './assets/sass/main.scss'

Vue.config.productionTip = false

require('./assets/scss/materialdesignicons.scss')
Vue.use(axios)
Vue.use(Buefy)

Vue.prototype.$backend = axios.create({
  baseURL: '/api/',
  withCredentials: true
})

Vue.prototype.$date = {
  fromLong: function (long) {
    return new Date(long)
  },
  toLong: function (date) {

  },
  format: function (date) {
    var dd = date.getDate()
    var mm = date.getMonth() + 1
    var yy = date.getFullYear()
    return '' + (dd > 9 ? dd : '0' + dd) +
      '.' + (mm > 9 ? mm : '0' + mm) +
      '.' + yy
  }

}
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
})

import Vue from 'vue'
import Router from 'vue-router'
import Archive from '@/components/routes/Archive'
import Drafts from '@/components/routes/Drafts'
import NewsEditor from '@/components/routes/NewsEditor'
import Main from '@/components/routes/Main'
import Login from '@/components/routes/Login'
import Settings from '@/components/routes/Settings'
import NewsletterEditor from '@/components/routes/NewsletterEditor'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/archive/:id?',
      name: '/archive',
      component: Archive
    },
    {
      path: '/settings',
      name: '/settings',
      component: Settings
    },
    {
      path: '/newseditor/:id?',
      name: '/newseditor',
      component: NewsEditor
    },
    {
      path: '/newslettereditor/:id?',
      name: '/newslettereditor',
      component: NewsletterEditor
    },
    {
      path: '/drafts',
      name: '/drafts',
      component: Drafts
    },
    {
      path: '/login',
      name: '/login',
      component: Login
    }
  ],
  mode: 'history'
})

package ru.softwerke.digest.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.softwerke.digest.entity.Editor;
import ru.softwerke.digest.service.model.EditorService;

import java.util.Collection;

@RestController
@RequestMapping("/api/editors")
public class EditorController {
    private final EditorService editorService;

    @Autowired
    public EditorController(EditorService editorService) {
        this.editorService = editorService;
    }

    @GetMapping
    public Collection<Editor> getAll() {
        return editorService.getAllEditors();
    }

    @PostMapping
    public ResponseEntity<Long> create(@RequestBody PostOrPatchRequest request) {
        long editorId = editorService.createEditor(request.getName(), request.getGroup());

        return new ResponseEntity<>(editorId, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity change(@PathVariable long id, @RequestBody PostOrPatchRequest request) {
        editorService.updateEditorGroup(id, request.getGroup());

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        editorService.disableEditor(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    private static class PostOrPatchRequest {
        private String name;
        private long group;

        String getName() {
            return name;
        }

        void setName(String name) {
            this.name = name;
        }

        long getGroup() {
            return group;
        }

        void setGroup(long group) {
            this.group = group;
        }
    }
}


package ru.softwerke.digest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.softwerke.digest.service.NewsletterAssemblyService;

import java.io.IOException;


@Controller
@RequestMapping("/api/newsletter_preview")
public class NewsletterPreviewController {
    private final NewsletterAssemblyService newsletterAssemblyService;

    @Autowired
    public NewsletterPreviewController(NewsletterAssemblyService newsletterAssemblyService) {
        this.newsletterAssemblyService = newsletterAssemblyService;
    }


    @GetMapping("/{id}")
    public ResponseEntity<String> preview(@PathVariable long id) throws IOException {
        String preview = newsletterAssemblyService.getNewsletter(id);

        return new ResponseEntity<>(preview, HttpStatus.OK);
    }
}

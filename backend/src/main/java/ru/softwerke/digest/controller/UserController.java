package ru.softwerke.digest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.softwerke.digest.service.AuthorizationService;

import java.security.Principal;

@RestController
@RequestMapping("/api/me")
public class UserController {
    private AuthorizationService authorizationService;

    @Autowired
    public UserController(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @GetMapping
    public UserInfo get(Principal principal) {
        if (principal == null) {
            return null;
        }

        String name = principal.getName();
        String role = "READER";


        if (authorizationService.isAdmin(name)) {
            role = "ADMIN";
        } else if (authorizationService.isEditor(name)) {
            role = "EDITOR";
        }

        return new UserInfo(name, role);
    }


    private static class UserInfo {
        private String name;
        private String role;


        private UserInfo(String name, String role) {
            this.name = name;
            this.role = role;
        }

        public String getName() {
            return name;
        }

        public String getRole() {
            return role;
        }
    }
}

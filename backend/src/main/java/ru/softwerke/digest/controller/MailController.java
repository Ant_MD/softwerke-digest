package ru.softwerke.digest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.softwerke.digest.entity.Mail;
import ru.softwerke.digest.service.model.MailService;

import java.util.Collection;

@RestController
@RequestMapping("/api/mails")
public class MailController {
    private final MailService mailService;

    @Autowired
    public MailController(MailService mailService) {
        this.mailService = mailService;
    }

    @GetMapping
    public Collection<Mail> getAll() {
        return mailService.getAll();
    }

    @PostMapping
    public ResponseEntity<Long> create(@RequestBody PostRequest request) {
        long id = mailService.create(request.getAddress());

        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        mailService.delete(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    private static class PostRequest {
        private String address;

        String getAddress() {
            return address;
        }

        private void setAddress(String address) {
            this.address = address;
        }
    }
}

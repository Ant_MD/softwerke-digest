package ru.softwerke.digest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.softwerke.digest.entity.Newsletter;
import ru.softwerke.digest.entity.NewsletterPart;
import ru.softwerke.digest.service.model.NewsletterPartService;
import ru.softwerke.digest.service.model.NewsletterService;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/newsletters")
public class NewsletterController {
    private final NewsletterService newsletterService;
    private final NewsletterPartService newsletterPartService;

    @Autowired
    public NewsletterController(NewsletterService newsletterService, NewsletterPartService newsletterPartService) {
        this.newsletterService = newsletterService;
        this.newsletterPartService = newsletterPartService;
    }

    @GetMapping
    public List<Newsletter> getAll() {
        return newsletterService.getAllNewsletters();
    }

    @GetMapping("/drafts")
    public Collection<Newsletter> getDrafts() {
        return newsletterService.getUnpublishedNewsletters();
    }

    @GetMapping("/{id}")
    public Newsletter get(@PathVariable long id) {
        return newsletterService.getNewsletter(id);
    }

    @PostMapping
    public ResponseEntity<Long> create(@RequestBody PostOrPatchNewsletterRequest request) {
        String title = request.getTitle();
        Date publicationDate = request.getPublicationDate();

        long newsletterId = newsletterService.createNewsletter(title, publicationDate);

        newsletterPartService.createPartsFromUnpublishedNews(newsletterId);

        return new ResponseEntity<>(newsletterId, HttpStatus.CREATED);
    }

    @PostMapping("/{id}/parts")
    public ResponseEntity<Long> createPart(@PathVariable long id, @RequestBody PostOrPatchNewsletterPartRequest request) {
        long partId = newsletterPartService.createNewsletterPart(id, request.getSource(), request.getTitle(), request.getContent(), request.getDate());
        return new ResponseEntity<>(partId, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}/parts/{part_id}")
    public ResponseEntity changePart(@PathVariable long id, @PathVariable(value = "part_id") long partId, @RequestBody PostOrPatchNewsletterPartRequest request) {
        newsletterPartService.updateNewsletterPart(partId, request.getTitle(), request.getContent(), request.getDate());

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}/parts")
    public Collection<NewsletterPart> getAllParts(@PathVariable long id) {
        return newsletterPartService.getAllNewsletterPartsForNewsletter(id);
    }

    @DeleteMapping("/{id}/parts/{part_id}")
    public ResponseEntity deletePart(@PathVariable long id, @PathVariable(value = "part_id") long partId) {
        newsletterPartService.deleteNewsletterPart(partId); // TODO: Проверить существование рассылки или вынести в отдеьный контроллер

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{id}")
    public ResponseEntity change(@PathVariable long id, @RequestBody PostOrPatchNewsletterRequest request) {
        String title = request.getTitle();
        Date publicationDate = request.getPublicationDate();

        newsletterService.updateNewsletter(id, title, publicationDate);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        newsletterService.deleteNewsletter(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    private static class PostOrPatchNewsletterRequest {
        private String title;
        private Date publicationDate;

        String getTitle() {
            return title;
        }

        void setTitle(String title) {
            this.title = title;
        }

        Date getPublicationDate() {
            return publicationDate;
        }

        void setPublicationDate(Date publicationDate) {
            this.publicationDate = publicationDate;
        }
    }

    private static class PostOrPatchNewsletterPartRequest {
        private String content;
        private String title;
        private Long source;
        private Date date;

        String getContent() {
            return content;
        }

        void setContent(String content) {
            this.content = content;
        }

        Long getSource() {
            return source;
        }

        void setSource(Long source) {
            this.source = source;
        }

        String getTitle() {
            return title;
        }

        void setTitle(String title) {
            this.title = title;
        }

        Date getDate() {
            return date;
        }

        void setDate(Date date) {
            this.date = date;
        }
    }
}

package ru.softwerke.digest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.softwerke.digest.service.model.*;

import java.security.Principal;
import java.util.Date;

@Deprecated
@RestController
@RequestMapping("/api/test")
public class TestController {
    private final NewsletterService newsletterService;
    private final NewsService newsService;
    private final TemplateService templateService;
    private final EditorService editorService;
    private final NewsletterPartService newsletterPartService;
    private final NewsGroupService newsGroupService;
    private final MailService mailService;

    @Autowired
    public TestController(NewsletterService newsletterService, NewsService newsService, TemplateService templateService, EditorService editorService, NewsletterPartService newsletterPartService, NewsGroupService newsGroupService, MailService mailService) {
        this.newsletterService = newsletterService;
        this.newsService = newsService;
        this.templateService = templateService;
        this.editorService = editorService;
        this.newsletterPartService = newsletterPartService;
        this.newsGroupService = newsGroupService;
        this.mailService = mailService;
    }

    @GetMapping("/a")
    public String a(Principal principal) {

        return principal.getClass().getName() + principal.getName();
    }

    @GetMapping
    public void test() {
        String templateBody =
                "<h1>{{title}}</h1>\n" +
                        "{{#news}}\n" +
                        "<h2>{{title}}</h2>\n" +
                        "<p>{{content}}</p>\n" +
                        "<hr />\n" +
                        "{{/news}}\n";

        templateService.setActiveTemplate(templateBody);

        long g1 = newsGroupService.createGroup("Group 1");
        long g2 = newsGroupService.createGroup("Group 2");
        long g3 = newsGroupService.createGroup("Group 3");

        long u1 = editorService.createEditor("alice", g1);
        long u2 = editorService.createEditor("bob", g2);
        long u3 = editorService.createEditor("carol", g3);
        long u4 = editorService.createEditor("dan", null);

        long n1 = newsService.createNews(u1, "Title 1", "Content 1", new Date());
        long n2 = newsService.createNews(u2, "Title 2", "Content 2", new Date());
        long n3 = newsService.createNews(u3, "Title 3", "Content 3", new Date());
        long nl1 = newsletterService.createNewsletter("Newsletter 1", new Date());

        long n4 = newsService.createNews(u1, "Title 4", "Content 4", new Date());
        long n5 = newsService.createNews(u2, "Title 5", "Content 5", new Date());
        long nl2 = newsletterService.createNewsletter("Newsletter 2", new Date());

        newsletterPartService.createNewsletterPart(nl1, null, "Title Extra", "Content Extra", new Date());
        newsletterPartService.createNewsletterPart(nl2, null, "Title Extra", "Content Extra", new Date());

        for (int i = 1; i <= 20; i++) {
            mailService.create("mail" + Integer.toString(i) + "@mail.test");
        }

    }
}

package ru.softwerke.digest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.softwerke.digest.entity.NewsItem;
import ru.softwerke.digest.service.model.EditorService;
import ru.softwerke.digest.service.model.NewsService;

import java.util.Collection;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/api/news")
public class NewsController {
    private final NewsService newsService;
    private final EditorService editorService;

    @Autowired
    public NewsController(NewsService newsService, EditorService editorService) {
        this.newsService = newsService;
        this.editorService = editorService;
    }

    @GetMapping
    public Collection<NewsItem> getAll() {
        return newsService.getAllNews();
    }

    @GetMapping("/available")
    public Collection<NewsItem> getAvailable(@RequestParam(value = "newsletter") long newsletterId) {
        return newsService.getAvailableNews(newsletterId);
    }

    @GetMapping("/published")
    public Collection<NewsItem> getPublished(@RequestParam(value = "contains", defaultValue = "") String contains) {
        return newsService.getPublishedNews(contains);
    }

    @GetMapping("/{id}")
    public NewsItem get(@PathVariable long id) {
        return newsService.getNews(id);
    }

    @PostMapping
    public ResponseEntity<Long> create(@RequestBody PostOrPatchRequest request) {
        String title = request.getTitle();
        String content = request.getContent();
        long creatorId = editorService.getEditorByName("bob").getId();          // TODO: получать из Principal
        Date date = request.getDate();

        long createdId = newsService.createNews(creatorId, title, content, date);

        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity change(@PathVariable long id, @RequestBody PostOrPatchRequest request) { // TODO
        String title = request.getTitle();
        String content = request.getContent();
        long creatorId = editorService.getEditorByName("bob").getId();          // TODO: получать из Principal
        Date date = request.getDate();
        Boolean active = request.isActive();

        newsService.updateNews(id, title, content, date, active);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    private static class PostOrPatchRequest {
        private String title;
        private String content;
        private Date date;
        private Boolean active;


        String getTitle() {
            return title;
        }

        private void setTitle(String title) {
            this.title = title;
        }

        String getContent() {
            return content;
        }

        private void setContent(String content) {
            this.content = content;
        }

        Date getDate() {
            return date;
        }

        private void setDate(Date date) {
            this.date = date;
        }

        Boolean isActive() {
            return active;
        }

        private void setActive(Boolean active) {
            this.active = active;
        }
    }
}

package ru.softwerke.digest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.softwerke.digest.entity.Template;
import ru.softwerke.digest.service.model.TemplateService;


@RestController
@RequestMapping("/api/template")
public class TemplateController {
    private final TemplateService templateService;

    @Autowired
    public TemplateController(TemplateService templateService) {
        this.templateService = templateService;
    }

    @GetMapping
    public Template get() {
        return templateService.getActiveTemplate();
    }

    @PatchMapping
    public ResponseEntity change(@RequestBody PatchRequest request) {
        templateService.setActiveTemplate(request.getBody());

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    static class PatchRequest {
        private String body;

        String getBody() {
            return body;
        }

        void setBody(String body) {
            this.body = body;
        }
    }
}

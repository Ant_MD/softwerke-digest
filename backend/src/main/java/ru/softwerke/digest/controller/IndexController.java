package ru.softwerke.digest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    @GetMapping(value = {"/", "/archive**", "/settings**", "/newseditor**", "/newslettereditor**", "/drafts**", "/login**"})
    public String get() {
        return "forward:/index.html";
    }
}

package ru.softwerke.digest.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.softwerke.digest.entity.NewsGroup;
import ru.softwerke.digest.service.model.NewsGroupService;

import java.util.List;

@RestController
@RequestMapping("/api/groups")
public class NewsGroupController {
    private final NewsGroupService newsGroupService;

    @Autowired
    public NewsGroupController(NewsGroupService newsGroupService) {
        this.newsGroupService = newsGroupService;
    }

    @GetMapping
    public List<NewsGroup> getAll(@RequestParam(defaultValue = "false") boolean all) {
        if (all) {
            return newsGroupService.getAllGroups();
        }

        return newsGroupService.getActiveGroups();
    }

    @PostMapping
    public ResponseEntity<Long> create(@RequestBody PostOrPatchRequest request) {
        long groupId = newsGroupService.createGroup(request.getName());

        return new ResponseEntity<>(groupId, HttpStatus.CREATED);
    }

    @PatchMapping("{id}")
    public ResponseEntity change(@PathVariable long id, @RequestBody PostOrPatchRequest request) {
        newsGroupService.updateGroup(id, request.getName(), request.isActive());

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    private static class PostOrPatchRequest {
        private String name;
        private boolean active;

        String getName() {
            return name;
        }

        private void setName(String name) {
            this.name = name;
        }

        boolean isActive() {
            return active;
        }

        private void setActive(boolean active) {
            this.active = active;
        }
    }
}


package ru.softwerke.digest.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

/**
 * Сущность описывающая редактора.
 */
@Entity
public class Editor {
    /**
     * Id редактора.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Имя редактора.
     */
    @Column(nullable = false, unique = true)
    private String name;

    /**
     * Состояние редактора: активен или неактивен.
     */
    @Column(nullable = false)
    private boolean active;

    /**
     * Наличие прав администратора у пользователя.
     */
    @Column(nullable = false)
    private boolean administrator;

    /**
     * Новостная группа в которую входит редактор.
     */
    @ManyToOne
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private NewsGroup group;

    @SuppressWarnings("unused")
    private Editor() {
    }

    public Editor(String name) {
        this.name = name;
        this.active = true;
    }

    public NewsGroup getGroup() {
        return group;
    }

    public void setGroup(NewsGroup newsGroup) {
        this.group = newsGroup;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }
}

package ru.softwerke.digest.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;

/**
 * Сущность описывающая часть новостной рассылки.
 */
@SuppressWarnings("unused")
@Entity
public class NewsletterPart {
    /**
     * Id части.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Заголовок части.
     */
    @Column(nullable = false)
    private String title;

    /**
     * Содержание части.
     */
    @Lob
    @Column(nullable = false)
    private String content;

    /**
     * Дата части.
     */
    @Column(nullable = false)
    private Date date;

    /**
     * Источник части.
     */
    @OneToOne
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private NewsItem source;

    /**
     * Рассылка, в которую входит данная часть.
     */
    @ManyToOne
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Newsletter newsletter;

    private NewsletterPart() {
    }

    public NewsletterPart(Newsletter newsletter) {
        this.newsletter = newsletter;
        this.title = "";
        this.date = new Date();
        this.content = "";
    }

    public NewsletterPart(Newsletter newsletter, NewsItem source) {
        this.newsletter = newsletter;
        this.source = source;
        this.title = source.getTitle();
        this.content = source.getContent();
        this.date = source.getDate();
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NewsItem getSource() {
        return source;
    }

    private void setSource(NewsItem source) {
        this.source = source;
    }

    public Newsletter getNewsletter() {
        return newsletter;
    }

    private void setNewsletter(Newsletter newsletter) {
        this.newsletter = newsletter;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

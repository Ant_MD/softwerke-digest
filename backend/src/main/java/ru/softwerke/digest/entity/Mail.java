package ru.softwerke.digest.entity;

import javax.persistence.*;

/**
 * Сущность описывающая почтовый адрес, на который буду отправляться рассылки.
 */
@Entity
public class Mail {
    /**
     * Id почтового адреса.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Почтовый адрес. Например: test@mail.test
     */
    @Column(nullable = false, unique = true)
    private String address;

    private Mail() {
    }

    public Mail(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    private void setAddress(String address) {
        this.address = address;
    }

    public long getId() {
        return id;
    }
}

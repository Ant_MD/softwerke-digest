package ru.softwerke.digest.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Сущность описывающая новостную рассылку.
 */
@Entity
public class Newsletter {
    /**
     * Id рассылки.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Заголовок рассылки
     */
    @Column(nullable = false)
    private String title;

    /**
     * Состояние публикаци.
     */
    @Column(nullable = false)
    private boolean published;

    /**
     * Запланированная дата публикации.
     */
    private Date publicationDate;

    /**
     * Состояние рассылки: автивна или нет.
     */
    @Column(nullable = false)
    private boolean isActive;

    /**
     * Множество частей рассылки.
     */
    @OneToMany(mappedBy = "newsletter", orphanRemoval = true)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private final Set<NewsletterPart> parts = new HashSet<>();

    /**
     * Множество получателей, которым будет отправлена данная рассылка.
     */
    @OneToMany(mappedBy = "newsletter", orphanRemoval = true)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private final Set<Receiver> receivers = new HashSet<>();

    @SuppressWarnings("unused")
    private Newsletter() {
    }

    public Newsletter(String title, Date publicationDate) {
        this.title = title;
        this.publicationDate = publicationDate;
        this.published = false;
        this.isActive = true;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Set<NewsletterPart> getParts() {
        return parts;
    }

    public Collection<Receiver> getReceivers() {
        return receivers;
    }
}

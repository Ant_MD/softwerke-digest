package ru.softwerke.digest.entity;

import javax.persistence.*;

/**
 * Сущность описывающая шаблон.
 */
@Entity
public class Template {
    /**
     * Id шаблона.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Тело шаблона.
     */
    @Lob
    @Column(nullable = false)
    private String body;

    public Template() {
        this.body = "";
    }

    public Long getId() {
        return id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

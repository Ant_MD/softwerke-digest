package ru.softwerke.digest.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Сущность описывающая новость.
 */
@Entity
public class NewsItem {
    /**
     * Id новости.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Заголовок новости.
     */
    @Column(nullable = false)
    private String title;

    /**
     * Дата новости.
     */
    @Column(nullable = false)
    private Date date;

    /**
     * Содержание новости
     */
    @Lob
    @Column(nullable = false)
    private String content;

    /**
     * Состояние новости: активна или нет.
     */
    @Column(nullable = false)
    private boolean active;

    /**
     * Множество частей рассылки, который ссылаются на эту новость.
     */
    @OneToMany(mappedBy = "source")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private final Set<NewsletterPart> newsletterParts = new HashSet<>();

    /**
     * Группа в которую входит данная новость.
     */
    @ManyToOne
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private NewsGroup group;

    @SuppressWarnings("unused")
    private NewsItem() {
    }

    public NewsItem(NewsGroup group, String title, String content, Date date) {
        this.group = group;
        this.title = title;
        this.content = content;
        this.date = date;
        this.active = true;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NewsGroup getGroup() {
        return group;
    }

    public void setGroup(NewsGroup newsGroup) {
        this.group = newsGroup;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    /**
     * Возвращает true если новость была опубликованна в одной из рассылок.
     */
    public boolean isPublished() {
        for (NewsletterPart newsletterPart : newsletterParts) {
            if (newsletterPart.getNewsletter().isPublished()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Возвращает true если, новость включена в рассылку.
     * @param newsletterId id рассылки
     */
    public boolean isIncludedInNewsletter(long newsletterId) {
        for (NewsletterPart part : newsletterParts) {
            Newsletter newsletter = part.getNewsletter();

            if (newsletter.getId() == newsletterId) {
                return true;
            }
        }

        return false;
    }
}

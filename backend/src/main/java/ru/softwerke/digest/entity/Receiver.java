package ru.softwerke.digest.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

/**
 * Сущность описывающая получателя рассылки.
 */
@Entity
public class Receiver {
    /**
     * Id получателя.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Статус отправки.
     */
    @Column(nullable = false)
    private boolean sent = false;

    /**
     * Адрес для отправки рассылкию
     */
    @Column(nullable = false)
    private String targetAddress;

    /**
     * Рассылка, которая должна быть отправлена
     */
    @ManyToOne
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Newsletter newsletter;

    private Receiver() {
    }

    public Receiver(String address, Newsletter newsletter) {
        this.targetAddress = address;
        this.newsletter = newsletter;
    }

    public String getTargetAddress() {
        return targetAddress;
    }

    private void setTargetAddress(String targetAddress) {
        this.targetAddress = targetAddress;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public long getId() {
        return id;
    }

    public Newsletter getNewsletter() {
        return newsletter;
    }
}

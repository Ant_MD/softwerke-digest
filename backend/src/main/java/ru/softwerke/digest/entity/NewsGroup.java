package ru.softwerke.digest.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Сущность описывающая новостную группу.
 */
@Entity
public class NewsGroup {
    /**
     * Id группы.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Имя группы.
     */
    @Column(nullable = false)
    private String name;

    /**
     * Состояние группы: активна или неактивна.
     */
    @Column(nullable = false)
    private boolean active;

    /**
     * Множество всех пользователй входящих в группу.
     */
    @OneToMany(mappedBy = "group")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Set<Editor> editors;

    /**
     * Множество всех новостей входящих в группу.
     */
    @OneToMany(mappedBy = "group")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Set<NewsItem> newsItems;

    @SuppressWarnings("unused")
    private NewsGroup() {
    }

    public NewsGroup(String name) {
        this.name = name;
        this.active = true;
        this.editors = new HashSet<>();
        this.newsItems = new HashSet<>();
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Editor> getEditors() {
        return editors;
    }

    public long getId() {
        return id;
    }

    public Set<NewsItem> getNewsItems() {
        return newsItems;
    }
}

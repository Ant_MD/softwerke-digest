package ru.softwerke.digest.service.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.softwerke.digest.entity.Template;
import ru.softwerke.digest.repository.TemplatesRepository;

import java.util.List;

/**
 * Сервис реализующий логику работы с шаблонами.
 */
@Service
@Transactional
public class TemplateService {
    private final TemplatesRepository templatesRepository;

    @Autowired
    public TemplateService(TemplatesRepository templatesRepository) {
        this.templatesRepository = templatesRepository;
    }

    /**
     * Возвращает активный шаблон.
     * @return шаблон
     */
    public Template getActiveTemplate() {
        return getSingleTemplate();
    }

    /**
     * Устанавливает тело шаблона.
     * @param templateBody
     */
    public void setActiveTemplate(String templateBody) {
        Template template = getSingleTemplate();
        template.setBody(templateBody);

        templatesRepository.save(template);
    }

    /**
     * Возвращает шаблон.
     * @return шаблон
     */
    public Template getOne() {
        return templatesRepository.findAll().get(0);
    }

    /**
     * Возвращает шаблон.
     * @return шаблон
     */
    private Template getSingleTemplate() {
        List<Template> templates = templatesRepository.findAll();

        return templates.isEmpty() ? new Template() : templates.get(0);
    }
}

package ru.softwerke.digest.service.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.softwerke.digest.entity.NewsGroup;
import ru.softwerke.digest.entity.NewsItem;
import ru.softwerke.digest.repository.NewsItemRepository;
import ru.softwerke.digest.service.model.exception.NewsItemNotFoundException;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Сервис реализующий логику работы с новостями.
 */
@Service
public class NewsService {
    private final NewsGroupService newsGroupService;
    private final NewsItemRepository newsItemRepository;

    @Autowired
    public NewsService(NewsItemRepository newsItemRepository, NewsGroupService newsGroupService) {
        this.newsItemRepository = newsItemRepository;
        this.newsGroupService = newsGroupService;
    }

    /**
     * Создаёт новую новость.
     * @param groupId id группы, в которой будет опубликована данная новость
     * @param title заголовок новости
     * @param content содержание новости
     * @param date дата новости
     * @return id новости
     */
    public long createNews(long groupId, String title, String content, Date date) {
        NewsGroup newsGroup = newsGroupService.getGroup(groupId);

        NewsItem newsItem = new NewsItem(newsGroup, title, content, date);

        return newsItemRepository.save(newsItem).getId();
    }

    /**
     * Изменяет указанную новость
     * @param id id новость
     * @param title заголовок новости
     * @param content содержание новости
     * @param date дата новости
     * @param active состояние новости
     */
    public void updateNews(long id, String title, String content, Date date, Boolean active) {
        NewsItem newsItem = newsItemRepository.findById(id).orElseThrow(NewsItemNotFoundException::new);

        newsItem.setTitle(title != null ? title : newsItem.getTitle());
        newsItem.setContent(content != null ? content : newsItem.getContent());
        newsItem.setDate(date != null ? date : newsItem.getDate());
        newsItem.setActive(active != null ? active : newsItem.getActive());

        newsItemRepository.save(newsItem);
    }

    /**
     * Возвращает новость с указанным id.
     * @param id id новости
     * @return новость
     */
    public NewsItem getNews(long id) {
        return newsItemRepository.findById(id).orElseThrow(NewsItemNotFoundException::new);
    }

    /**
     * Возвращает все новости
     * @return
     */
    public Collection<NewsItem> getAllNews() {
        return newsItemRepository.findAll();
    }

    /**
     * Возвращет все опубликованные новости.
     * Если contains != null, возвращает все новости, в которых встречается указанная строка.
     * @param contains строка для поиска
     * @return списко новостей
     */
    public Collection<NewsItem> getPublishedNews(String contains) {
        List<NewsItem> news = newsItemRepository.findAll();

        news.removeIf(item -> !item.isPublished());

        if (contains != null) {
            news.removeIf(item -> !item.getContent().contains(contains));
        }

        return news;
    }

    /**
     * Возвращает все доступные новости для рассыки. Доступные новости - включённые в указанную рассылку и неопубликованные.
     * @param newsletterId id рассылки
     * @return доступные новости
     */
    public Collection<NewsItem> getAvailableNews(long newsletterId) {
        List<NewsItem> news = newsItemRepository.findAll();
        news.removeIf(item -> item.isPublished() && !item.isIncludedInNewsletter(newsletterId));

        return news;
    }
}

package ru.softwerke.digest.service.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.softwerke.digest.entity.NewsItem;
import ru.softwerke.digest.entity.Newsletter;
import ru.softwerke.digest.entity.NewsletterPart;
import ru.softwerke.digest.repository.NewsletterPartRepository;
import ru.softwerke.digest.service.model.exception.NewsletterPartNotFoundException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Сервис реализующий логику работы с частями рассылки.
 */
@Service
public class NewsletterPartService {
    private final NewsService newsService;
    private final NewsletterService newsletterService;
    private final NewsletterPartRepository newsletterPartRepository;

    @Autowired
    public NewsletterPartService(NewsService newsService, NewsletterService newsletterService, NewsletterPartRepository newsletterPartRepository) {
        this.newsService = newsService;
        this.newsletterService = newsletterService;
        this.newsletterPartRepository = newsletterPartRepository;
    }

    /**
     * Создаёт новую часть рассылки. Если newsId == null, то title, content, date будут взяты из источника.
     * @param newsletterId id рассылки, в которую будет входить создаваемая часть
     * @param newsId id новости, на которую может ссылаться часть
     * @param title заголовок части
     * @param content содержание части
     * @param date дата части
     * @return id созданой части
     */
    public long createNewsletterPart(long newsletterId, Long newsId, String title, String content, Date date) {
        Newsletter newsletter = newsletterService.getNewsletter(newsletterId);

        NewsletterPart newsletterPart;

        if (newsId != null) {
            NewsItem news = newsService.getNews(newsId);
            newsletterPart = new NewsletterPart(newsletter, news);
        } else {
            newsletterPart = new NewsletterPart(newsletter);
        }

        if (title != null) {
            newsletterPart.setTitle(title);
        }

        if (content != null) {
            newsletterPart.setContent(content);
        }

        if (date != null) {
            newsletterPart.setDate(date);
        }

        return newsletterPartRepository.save(newsletterPart).getId();
    }

    /**
     * Создаёт части рассылки из неопубликованных новостей для указанной рассылки.
     * @param newsletterId id рассылки
     */
    public void createPartsFromUnpublishedNews(long newsletterId) {
        Newsletter newsletter = newsletterService.getNewsletter(newsletterId);

        Collection<NewsItem> unpublishedNews = newsService.getAvailableNews(newsletterId);

        List<NewsletterPart> parts = new ArrayList<>();

        for (NewsItem news : unpublishedNews) {
            NewsletterPart part = new NewsletterPart(newsletter, news);

            parts.add(part);
        }

        newsletterPartRepository.saveAll(parts);
    }

    /**
     * Изменяет часть с указанным id.
     * @param newsletterPartId id рассылки
     * @param title заголовок части
     * @param content содержание части
     * @param date дата части
     */
    public void updateNewsletterPart(long newsletterPartId, String title, String content, Date date) {
        NewsletterPart newsletterPart = newsletterPartRepository.findById(newsletterPartId).orElseThrow(NewsletterPartNotFoundException::new);

        newsletterPart.setContent(content);
        newsletterPart.setTitle(title);
        newsletterPart.setDate(date);

        newsletterPartRepository.save(newsletterPart);
    }

    /**
     * Удаляет часть с указанным id.
     * @param newsletterPartId id части
     */
    public void deleteNewsletterPart(long newsletterPartId) {
        NewsletterPart newsletterPart = newsletterPartRepository.findById(newsletterPartId).orElseThrow(NewsletterPartNotFoundException::new);

        newsletterPartRepository.delete(newsletterPart);
    }

    /**
     * Возвращает список всех частей для указанной рассылки.
     * @param newsletterId id рассылки
     * @return части рассылки
     */
    public Collection<NewsletterPart> getAllNewsletterPartsForNewsletter(long newsletterId) {
        Newsletter newsletter = newsletterService.getNewsletter(newsletterId);

        return newsletterPartRepository.findAllByNewsletterEquals(newsletter);
    }
}

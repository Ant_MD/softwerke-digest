package ru.softwerke.digest.service.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.softwerke.digest.ApplicationProperties;
import ru.softwerke.digest.entity.Editor;
import ru.softwerke.digest.entity.NewsGroup;
import ru.softwerke.digest.repository.EditorRepository;
import ru.softwerke.digest.service.model.exception.EditorNotFoundException;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Сервис реализующий логику работы с редакторами
 */
@Service
public class EditorService {
    private final EditorRepository editorRepository;
    private final NewsGroupService newsGroupService;
    private final ApplicationProperties applicationProperties;

    @Autowired
    public EditorService(EditorRepository editorRepository, NewsGroupService newsGroupService, ApplicationProperties applicationProperties) {
        this.editorRepository = editorRepository;
        this.newsGroupService = newsGroupService;
        this.applicationProperties = applicationProperties;
    }

    /**
     * Создаёт пользователя с правами администратора при инициализации. Имя пользователя берётся из параметров.
     */
    @PostConstruct
    private void postConstruct() {
        String name = applicationProperties.getAdminUid();

        if(name!= null && !name.isEmpty()) {
            createAdministrator(name);
        }
    }


    /**
     * Создаёт пользователя с правами администратора.
     * @param name имя нового пользователя
     * @return id нового пользователя
     */
    private long createAdministrator(String name) {

        Collection<Editor> editors = editorRepository.findAll();

        for(Editor editor: editors) {
            if(editor.getName().equals(name)){
                editor.setAdministrator(true);
                editor.setActive(true);

                return editorRepository.save(editor).getId();
            }
        }

        Editor editor = new Editor(name);
        editor.setGroup(newsGroupService.getDefaultGroup());
        editor.setAdministrator(true);
        editor.setActive(true);

        return editorRepository.save(editor).getId();
    }

    /**
     * Возвращает пользователя с указанным id.
     * @param id id редактора
     * @return редактор
     */
    public Editor getEditor(long id) {
        return editorRepository.findById(id).orElseThrow(EditorNotFoundException::new);
    }

    /**
     * Возвращает пользователя с указанным именем.
     * @param name имя редактора
     * @return редактор
     */
    public Editor getEditorByName(String name) {
        return editorRepository.findByName(name).orElseThrow(EditorNotFoundException::new);
    }

    /**
     * Возвращает всех пользователей.
     * @return список всех пользователей
     */
    public Collection<Editor> getAllEditors() {
        return editorRepository.findAll();
    }

    /**
     * Возвращает всех активных пользователей.
     * @return список всех активных пользователей
     */
    public Collection<Editor> getActiveEditors() {
        return editorRepository.findAllByActive(true);
    }

    /**
     * Возвращает всех активных администраторов.
     * @return список всех пользователей
     */
    public Collection<Editor> getActiveAdministrators() {
        return editorRepository.findAllByAdministrator(true);
    }

    /**
     * Создаёт нового пользователя пользователя. Если группа пользователя не указана, то он добавляется в стандартную.
     * @param name имя нового пользователя
     * @param groupId группа
     * @return id нового пользователя
     */
    public long createEditor(String name, Long groupId) {
        Optional<Editor> editorOptional = editorRepository.findByName(name);

        NewsGroup group;
        if (groupId == null) {
            group = newsGroupService.getDefaultGroup();
        } else {
            group = newsGroupService.getGroup(groupId);
        }

        if (editorOptional.isPresent()) {
            Editor editor = editorOptional.get();
            editor.setActive(true);
            editor.setGroup(group);
            return editorRepository.save(editor).getId();
        }

        Editor editor = new Editor(name);
        editor.setGroup(group);

        return editorRepository.save(editor).getId();
    }

    /**
     * Изменяет группу редактора.
     * @param id id редактора
     * @param groupId id группы
     */
    public void updateEditorGroup(long id, long groupId) {
        Editor editor = editorRepository.findById(id).orElseThrow(EditorNotFoundException::new);
        NewsGroup group = newsGroupService.getGroup(groupId);

        editor.setGroup(group);

        editorRepository.save(editor);
    }

    /**
     * Деактивирует редактора.
     * @param id id редактора
     */
    public void disableEditor(long id) {
        Editor editor = editorRepository.findById(id).orElseThrow(EditorNotFoundException::new);

        editor.setActive(false);

        editorRepository.save(editor);
    }
}

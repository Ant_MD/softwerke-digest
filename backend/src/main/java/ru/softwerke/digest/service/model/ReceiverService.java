package ru.softwerke.digest.service.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.softwerke.digest.entity.Newsletter;
import ru.softwerke.digest.entity.Receiver;
import ru.softwerke.digest.repository.ReceiverRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Сервис реализующий логику работы с получателями.
 */
@Service
@Transactional
public class ReceiverService {
    private final ReceiverRepository receiverRepository;
    private final NewsletterService newsletterService;

    @Autowired
    public ReceiverService(ReceiverRepository receiverRepository, NewsletterService newsletterService) {
        this.receiverRepository = receiverRepository;
        this.newsletterService = newsletterService;
    }

    /**
     * Создаёт множество получателей для указанной рассылки из переданных адресов.
     * @param newsletterId id рассылки
     * @param addresses список адресов
     */
    public void createMany(long newsletterId, Collection<String> addresses) {
        // TODO: Проверить на существование дубликатов при добавке

        Newsletter newsletter = newsletterService.getNewsletter(newsletterId);

        List<Receiver> mails = new ArrayList<>(addresses.size());

        for (String address : addresses) {
            mails.add(new Receiver(address, newsletter));
        }

        receiverRepository.saveAll(mails);
    }

    /**
     * Возвращает получателя с указанным id.
     * @param id id получателя
     * @return получатель
     */
    public Receiver get(long id) {
        return receiverRepository.findById(id).get(); // TODO: Обработать ошибку
    }

    /**
     * Возвращает всех получателей.
     * @return все получатели
     */
    public Collection<Receiver> getAll() {
        return receiverRepository.findAll();
    }

    /**
     * Возвращает список всех неотправленных получателей.
     * @return получатели
     */
    public Collection<Receiver> getUnsended() {
        return receiverRepository.findAllBySent(false);
    }

    /**
     * Устанавливает статус отправки.
     * @param id id получателя
     * @param sent статус отправки
     */
    public void setSent(long id, boolean sent) {
        Receiver receiver = receiverRepository.findById(id).get(); // TODO: Обработать ошибку

        receiver.setSent(sent);

        receiverRepository.save(receiver);
    }

}

package ru.softwerke.digest.service.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.softwerke.digest.entity.NewsGroup;
import ru.softwerke.digest.repository.NewsGroupRepository;
import ru.softwerke.digest.service.model.exception.BadNewsGroupNameException;
import ru.softwerke.digest.service.model.exception.NewsGroupNotFoundException;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

/**
 * Сервис реализующий логику работы с новостными группами.
 */
@Service
public class NewsGroupService {
    private final NewsGroupRepository newsGroupRepository;

    private final String DEFAULT_GROUP_NAME = "-";

    @Autowired
    public NewsGroupService(NewsGroupRepository newsGroupRepository) {
        this.newsGroupRepository = newsGroupRepository;
    }

    /**
     * Создаёт стандартную группу при инициализации.
     */
    @PostConstruct
    private void postConstruct() {
        createDefaultGroup();
    }

    /**
     * Создаёт новую группу.
     * @param name имя группы
     * @return id новой группы
     */
    public long createGroup(String name) {
        if (!checkName(name)) {
            throw new BadNewsGroupNameException();
        }

        Optional<NewsGroup> maybeOldGroup = newsGroupRepository.findByName(name);

        NewsGroup group = maybeOldGroup.orElseGet(() -> new NewsGroup(name));
        group.setActive(true);

        return newsGroupRepository.save(group).getId();
    }

    /**
     * Отключает группу
     * @param id id отключаемой группы
     */
    public void disableGroup(long id) {
        NewsGroup group = newsGroupRepository.findById(id).orElseThrow(NewsGroupNotFoundException::new);

        if (group.getName().equals(DEFAULT_GROUP_NAME)) {
            throw new BadNewsGroupNameException();
        }

        if (group.getEditors().isEmpty() && group.getNewsItems().isEmpty()) { // TODO: Возможно стоит вынести в функцию по таймеру
            newsGroupRepository.delete(group);
        } else {
            group.setActive(false);

            newsGroupRepository.save(group);
        }
    }

    /**
     * Возвращает группу с указанным id.
     * @param id id группы
     * @return группа
     */
    public NewsGroup getGroup(long id) {
        return newsGroupRepository.findById(id).orElseThrow(NewsGroupNotFoundException::new);
    }

    public List<NewsGroup> getAllGroups() {
        return newsGroupRepository.findAll();
    }

    public List<NewsGroup> getActiveGroups() {
        return newsGroupRepository.findByActiveIsTrue();
    }

    public void updateGroup(long id, String name, boolean active) {
        if (!checkName(name)) {
            throw new BadNewsGroupNameException();
        }

        NewsGroup group = newsGroupRepository.findById(id).orElseThrow(NewsGroupNotFoundException::new);
        group.setName(name);
        group.setActive(active); // TODO: удалить всех пользователей из группы

        newsGroupRepository.save(group);
    }

    /**
     * Возвращает стандартную группу.
     * @return стандартная группа
     */
    public NewsGroup getDefaultGroup() {
        return newsGroupRepository.findByName(DEFAULT_GROUP_NAME).orElseThrow(NewsGroupNotFoundException::new);
    }

    /**
     * Создаёт стандартную группу, если она была ещё не создана.
     */
    private void createDefaultGroup() {
        Optional<NewsGroup> maybeNewsGroup = newsGroupRepository.findByName(DEFAULT_GROUP_NAME);

        if (maybeNewsGroup.isPresent()) {
            NewsGroup newsGroup = maybeNewsGroup.get();
            newsGroup.setActive(true);

            newsGroupRepository.save(newsGroup);
        } else {
            NewsGroup newsGroup = new NewsGroup(DEFAULT_GROUP_NAME);

            newsGroupRepository.save(newsGroup);
        }
    }

    /**
     * Проверяет имя группы на соответствие требованиям.
     * @param name имя группы
     * @return true, если имя соответствует требованиям
     */
    private boolean checkName(String name) {
        int MAX_GROUP_NAME_LENGTH = 100;

        boolean validLength = name.length() > 0 && name.length() < MAX_GROUP_NAME_LENGTH;
        boolean notDefault = !name.equals(DEFAULT_GROUP_NAME);

        return validLength && notDefault;
    }
}

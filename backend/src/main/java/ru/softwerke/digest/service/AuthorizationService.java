package ru.softwerke.digest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.softwerke.digest.entity.Editor;
import ru.softwerke.digest.service.model.EditorService;

import java.util.Collection;

/**
 * Сервис отвечающий за авторизацию.
 */
@Service
@Transactional
public class AuthorizationService {
    private final EditorService editorService;

    @Autowired
    public AuthorizationService(EditorService editorService) {
        this.editorService = editorService;
    }

    /**
     * Возвращает true, если пользователь является администратором.
     * @param login имя пользователя
     */
    public boolean isAdmin(String login) {
        Collection<Editor> editors = editorService.getActiveAdministrators();

        for (Editor editor : editors) {
            if (login.equals(editor.getName())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Возвращает true, если пользователь является редактором.
     * @param login имя пользователя
     */
    public boolean isEditor(String login) {
        Collection<Editor> editors = editorService.getActiveEditors();

        for (Editor editor : editors) {
            if (login.equals(editor.getName())) {
                return true;
            }
        }

        return false;
    }
}

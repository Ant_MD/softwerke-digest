package ru.softwerke.digest.service;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.softwerke.digest.entity.Newsletter;
import ru.softwerke.digest.entity.NewsletterPart;
import ru.softwerke.digest.entity.Template;
import ru.softwerke.digest.service.model.NewsletterService;
import ru.softwerke.digest.service.model.TemplateService;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Сервис отвечающий за компоновку рассылки
 */
@Service
@Transactional
public class NewsletterAssemblyService {
    private final TemplateService templateService;
    private final NewsletterService newsletterService;

    @Autowired
    public NewsletterAssemblyService(TemplateService templateService, NewsletterService newsletterService) {
        this.templateService = templateService;
        this.newsletterService = newsletterService;
    }

    /**
     * Возвращает скомпонованную рассылку.
     * @param id id рассылки
     * @return скомпонованная рассылка
     * @throws IOException
     */
    public String getNewsletter(long id) throws IOException {
        Template template = templateService.getOne();

        Newsletter newsletter = newsletterService.getNewsletter(id);

        MustacheFactory mustacheFactory = new DefaultMustacheFactory();
        Mustache m = mustacheFactory.compile(new StringReader(template.getBody()), "template_name");

        StringWriter stringWriter = new StringWriter();
        m.execute(stringWriter, new TemplateNewsletter(newsletter)).flush(); // TODO: обработать исключение

        return stringWriter.toString();
    }

    /**
     * Проверяет заданный шаблон на корректность.
     * @param template шаблон
     * @return true, если шаблон корректен
     */
    public boolean checkTemplate(String template) {
        // TODO: реализовать проверку шаблона
        return true;
    }

    class TemplateNewsletter {
        private final String title;
        private List<TemplatePart> news;

        TemplateNewsletter(Newsletter newsletter) {
            this.title = newsletter.getTitle();

            this.news = new ArrayList<>();

            for (NewsletterPart part : newsletter.getParts()) {
                news.add(new TemplatePart(part));
            }
        }

        String title() {
            return title;
        }

        List<TemplatePart> news() {
            return news;
        }
    }

    @SuppressWarnings("unused")
    private class TemplatePart {
        private NewsletterPart newsletterPart;

        TemplatePart(NewsletterPart part) {
            this.newsletterPart = part;
        }

        String title() {
            return newsletterPart.getTitle();
        }

        String content() {
            return newsletterPart.getContent();
        }

        String date() { return new SimpleDateFormat("dd.mm.yyyy").format(newsletterPart.getDate());}
    }
}

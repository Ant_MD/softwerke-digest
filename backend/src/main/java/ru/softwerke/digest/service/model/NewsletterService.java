package ru.softwerke.digest.service.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.softwerke.digest.entity.Newsletter;
import ru.softwerke.digest.repository.NewsletterRepository;
import ru.softwerke.digest.service.model.exception.NewsletterNotFoundException;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Сервис реализующий логику работы с рассылками.
 */
@Service
@Transactional
public class NewsletterService {
    private final NewsletterRepository newsletterRepository;

    @Autowired
    public NewsletterService(NewsletterRepository newsletterRepository) {
        this.newsletterRepository = newsletterRepository;
    }

    /**
     * Создаёт новую рассылку.
     * @param title заголовок рассылки
     * @param publicationDate дата публикации рассылки
     * @return id новой рассылки
     */
    public long createNewsletter(String title, Date publicationDate) {
        Newsletter newsletter = new Newsletter(title, publicationDate);

        return newsletterRepository.save(newsletter).getId();
    }

    /**
     * Изменяет рассылку.
     * @param id id рассылки
     * @param title заголовок рассылки
     * @param publicationDate дата публикации рассылки
     */
    public void updateNewsletter(long id, String title, Date publicationDate) {
        Newsletter newsletter = newsletterRepository.findById(id).orElseThrow(NewsletterNotFoundException::new);

        newsletter.setTitle(title);
        newsletter.setPublicationDate(publicationDate);

        newsletterRepository.save(newsletter);
    }

    /**
     * Возвращает рассылку с указанным id.
     * @param id id рассылки
     * @return рассылка
     */
    public Newsletter getNewsletter(long id) {
        return newsletterRepository.findById(id).orElseThrow(NewsletterNotFoundException::new);
    }

    /**
     * Возвращает все рассылки.
     * @return рассылки
     */
    public List<Newsletter> getAllNewsletters() {
        return newsletterRepository.findAll();
    }

    /**
     * Возвращает все неопубликованные рассылки.
     * @return неопубликованные рассылки
     */
    public Collection<Newsletter> getUnpublishedNewsletters() {
        return newsletterRepository.findAllByPublishedIsFalse();
    }

    /**
     * Удаляет рассылку.
     * @param id id рассылки
     */
    public void deleteNewsletter(long id) {
        Newsletter newsletter = newsletterRepository.findById(id).orElseThrow(NewsletterNotFoundException::new);

        newsletterRepository.delete(newsletter);
    }

    /**
     * Изменяет состояние публикации рассылки.
     * @param id id рассылки
     * @param published состояние публикации
     */
    public void setPublished(long id, boolean published) {
        Newsletter newsletter = newsletterRepository.findById(id).orElseThrow(NewsletterNotFoundException::new);

        newsletter.setPublished(published);

        newsletterRepository.save(newsletter);
    }

}

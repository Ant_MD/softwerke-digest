package ru.softwerke.digest.service.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.softwerke.digest.entity.Mail;
import ru.softwerke.digest.repository.MailRepository;
import ru.softwerke.digest.service.model.exception.MailAlreadyExistException;

import java.util.Collection;

/**
 * Сервис отвечающий за работу с почтовыми адресами
 */
@Service
@Transactional
public class MailService {
    private final MailRepository mailRepository;

    @Autowired
    public MailService(MailRepository mailRepository) {
        this.mailRepository = mailRepository;
    }

    /**
     * Создаёт почтовый адрес.
     * @param address почтовый адрес
     * @return id нового или уже существующего адреса
     */
    public long create(String address) {
        Collection<Mail> mails = mailRepository.findAll();

        for (Mail mail : mails) {
            if (mail.getAddress().equals(address)) {
                throw new MailAlreadyExistException();
            }
        }

        return mailRepository.save(new Mail(address)).getId();
    }

    /**
     * Удаляет почтовый адрес.
     * @param id id удаляемого адреса
     */
    public void delete(long id) {
        mailRepository.deleteById(id);
    }

    /**
     * Возвращает список всех почтовых адресов.
     * @return список всех почтовых адресов
     */
    public Collection<Mail> getAll() {
        return mailRepository.findAll();
    }
}

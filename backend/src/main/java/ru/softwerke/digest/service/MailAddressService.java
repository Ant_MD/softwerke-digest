package ru.softwerke.digest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.softwerke.digest.entity.Mail;
import ru.softwerke.digest.service.model.MailService;
import ru.softwerke.digest.util.ldap.LdapPerson;
import ru.softwerke.digest.util.ldap.LdapPersonMapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Сервис отвечающий за получение адресов для рассылки.
 */
@Service
@Transactional
public class MailAddressService {
    private final MailService mailService;
    private final LdapTemplate ldapTemplate;

    @Autowired
    public MailAddressService(MailService mailService, LdapTemplate ldapTemplate) {
        this.mailService = mailService;
        this.ldapTemplate = ldapTemplate;
    }

    /**
     * Возвращает все дополнительные адреса.
     * @return почтовые адреса
     */
    private Collection<String> getExtraAddresses() {
        List<String> addresses = new ArrayList<>();

        for (Mail mail : mailService.getAll()) {
            addresses.add(mail.getAddress());
        }

        return addresses;
    }

    /**
     * Возвращает все адреса, полученные через ldap.
     * @return почтовые адреса
     */
    private Collection<String> getLdapAddresses() {
        // TODO: Проверить рекурсивную загрузку
        LdapQuery query = LdapQueryBuilder.query()
                .where("objectClass").is("Person");

        Collection<LdapPerson> people = ldapTemplate.search(query, new LdapPersonMapper());

        List<String> addresses = new ArrayList<>(people.size());

        for (LdapPerson person : people) {
            if (person.getMail() != null) {
                addresses.add(person.getMail());
            }
        }

        return addresses;
    }

    /**
     * Возвращает все доступные адреса
     * @return почтовые адреса
     */
    public Collection<String> getMailAddresses() {
        Collection<String> addresses = getExtraAddresses();
        addresses.addAll(getLdapAddresses());

        return addresses;
    }
}

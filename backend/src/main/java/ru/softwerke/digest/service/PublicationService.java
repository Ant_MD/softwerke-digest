package ru.softwerke.digest.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.softwerke.digest.ApplicationProperties;
import ru.softwerke.digest.entity.Newsletter;
import ru.softwerke.digest.entity.Receiver;
import ru.softwerke.digest.service.model.NewsletterService;
import ru.softwerke.digest.service.model.ReceiverService;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Сервис отвечающий за публикацию рассылок.
 */
@Service
public class PublicationService {
    private final NewsletterAssemblyService newsletterAssemblyService;
    private final NewsletterService newsletterService;
    private final MailAddressService mailAddressService;
    private final ReceiverService receiverService;
    private final JavaMailSender javaMailSender;
    private final ApplicationProperties applicationProperties;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public PublicationService(NewsletterAssemblyService newsletterAssemblyService, NewsletterService newsletterService, MailAddressService mailAddressService, ReceiverService receiverService, JavaMailSender javaMailSender, ApplicationProperties applicationProperties) {
        this.newsletterAssemblyService = newsletterAssemblyService;
        this.newsletterService = newsletterService;
        this.mailAddressService = mailAddressService;
        this.receiverService = receiverService;
        this.javaMailSender = javaMailSender;
        this.applicationProperties = applicationProperties;
    }

    /**
     * Отмечает подходящие рассылки как опубликованные и создаёт получателей для отправки.
     */
    @Scheduled(fixedDelay = 60_000)
    private void publishPlanedNewsletters() {
        logger.info("Check unpublished newsletters");

        Date currentDate = new Date();

        Collection<Newsletter> newsletters = newsletterService.getUnpublishedNewsletters();

        for (Newsletter newsletter : newsletters) {
            if (newsletter.getPublicationDate() == null) {
                continue;
            }

            if (currentDate.after(newsletter.getPublicationDate())) {
                logger.info("Publish newsletter: " + newsletter.getTitle());

                publishNewsletter(newsletter.getId());
            }
        }
    }

    /**
     * Отправляет письма получателям.
     */
    @Scheduled(fixedDelay = 60_000)
    private void sendAll() {
        Collection<Receiver> receivers = receiverService.getUnsended();

        for (Receiver receiver : receivers) {
            try {
                sendMail(receiver.getId());
                TimeUnit.MILLISECONDS.sleep(applicationProperties.getMail().getDelay());
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            }
        }
    }

    @Transactional
    protected void publishNewsletter(long id) {
        Collection<String> addresses = mailAddressService.getMailAddresses();

        receiverService.createMany(id, addresses);

        newsletterService.setPublished(id, true);
    }

    @Transactional
    protected void sendMail(long receiverId) {
        // TODO: обработка исключений

        Receiver receiver = receiverService.get(receiverId);

        logger.info("Send mail to: " + receiver.getTargetAddress());

        try {
            String text = newsletterAssemblyService.getNewsletter(receiver.getNewsletter().getId());

            MimeMessage message = javaMailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setText(text, true);
            helper.setTo(receiver.getTargetAddress());
            helper.setFrom(applicationProperties.getMail().getFrom());


            javaMailSender.send(message);

            receiverService.setSent(receiver.getId(), true);

        } catch (IOException | MessagingException e) {
            e.printStackTrace();
        }
    }
}

package ru.softwerke.digest.util.ldap;

public class LdapPerson {
    private final String mail;

    LdapPerson(String mail) {
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }
}

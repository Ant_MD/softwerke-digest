package ru.softwerke.digest.util.ldap;

import org.springframework.ldap.core.AttributesMapper;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

public class LdapPersonMapper implements AttributesMapper<LdapPerson> {
    @Override
    public LdapPerson mapFromAttributes(Attributes attributes) throws NamingException {
        Attribute maybeMail = attributes.get("mail");

        final String mail;

        if (maybeMail != null) {
            mail = (String) maybeMail.get();
        } else {
            mail = null;
        }

        return new LdapPerson(mail);
    }
}

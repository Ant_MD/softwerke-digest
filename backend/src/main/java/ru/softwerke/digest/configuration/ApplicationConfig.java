package ru.softwerke.digest.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.softwerke.digest.ApplicationProperties;

@Configuration
@EnableScheduling
public class ApplicationConfig {
    private ApplicationProperties applicationProperties;

    @Autowired
    public ApplicationConfig(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Bean
    public LdapContextSource contextSource() {
        LdapContextSource contextSource = new LdapContextSource();

        contextSource.setUrl("ldap://" + applicationProperties.getLdap().getIp() + ":" + applicationProperties.getLdap().getPort());
        contextSource.setBase(applicationProperties.getLdap().getBase());

        return contextSource;
    }

    @Bean
    public LdapTemplate ldapTemplate() {
        return new LdapTemplate(contextSource());
    }
}

package ru.softwerke.digest.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.LdapShaPasswordEncoder;
import ru.softwerke.digest.ApplicationProperties;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private ApplicationProperties applicationProperties;

    @Autowired
    public WebSecurityConfig(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/static/**").permitAll()
                .anyRequest().authenticated()
                .and().formLogin().loginPage("/login").permitAll();
        http.csrf().disable();
        http.headers().frameOptions().disable(); // TODO: sameOrigin()
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        // TODO: Проверять подключение к mail и ldap серверам
        auth.ldapAuthentication()
                .userDnPatterns("uid={0},ou=people")
                .contextSource()
                .url("ldap://" + applicationProperties.getLdap().getIp() + ":" + applicationProperties.getLdap().getPort() + "/" + applicationProperties.getLdap().getBase())
                .and()
                .passwordCompare()
                .passwordEncoder(new LdapShaPasswordEncoder()) // TODO: Добавить настройку алгоритма
                .passwordAttribute("userPassword");

    }
}

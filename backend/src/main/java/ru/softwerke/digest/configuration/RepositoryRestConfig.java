package ru.softwerke.digest.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.mapping.RepositoryDetectionStrategy;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import ru.softwerke.digest.entity.Editor;
import ru.softwerke.digest.entity.NewsGroup;
import ru.softwerke.digest.entity.NewsItem;
import ru.softwerke.digest.entity.Newsletter;

@Configuration
public class RepositoryRestConfig extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.setRepositoryDetectionStrategy(RepositoryDetectionStrategy.RepositoryDetectionStrategies.ANNOTATED);
        config.exposeIdsFor(NewsItem.class, Newsletter.class, Editor.class, NewsGroup.class);
    }
}

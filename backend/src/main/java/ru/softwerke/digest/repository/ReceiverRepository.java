package ru.softwerke.digest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.softwerke.digest.entity.Receiver;

import java.util.List;

public interface ReceiverRepository extends JpaRepository<Receiver, Long> {
    List<Receiver> findAllBySent(boolean sent);
}

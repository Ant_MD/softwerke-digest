package ru.softwerke.digest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.softwerke.digest.entity.Newsletter;

import java.util.List;

public interface NewsletterRepository extends JpaRepository<Newsletter, Long> {
    List<Newsletter> findAllByPublishedIsFalse();
}

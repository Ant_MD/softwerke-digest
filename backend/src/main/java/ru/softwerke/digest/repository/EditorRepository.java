package ru.softwerke.digest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.softwerke.digest.entity.Editor;

import java.util.List;
import java.util.Optional;

public interface EditorRepository extends JpaRepository<Editor, Long> {
    Optional<Editor> findByName(String name);

    List<Editor> findAllByActive(boolean active);

    List<Editor> findAllByAdministrator(boolean administrator);
}

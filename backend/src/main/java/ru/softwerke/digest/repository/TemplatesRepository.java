package ru.softwerke.digest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.softwerke.digest.entity.Template;

public interface TemplatesRepository extends JpaRepository<Template, Long> {
}

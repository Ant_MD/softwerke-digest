package ru.softwerke.digest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.softwerke.digest.entity.Newsletter;
import ru.softwerke.digest.entity.NewsletterPart;

import java.util.List;

public interface NewsletterPartRepository extends JpaRepository<NewsletterPart, Long> {
    List<NewsletterPart> findAllByNewsletterEquals(Newsletter newsletter);
}

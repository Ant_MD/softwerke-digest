package ru.softwerke.digest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.softwerke.digest.entity.NewsGroup;

import java.util.List;
import java.util.Optional;

public interface NewsGroupRepository extends JpaRepository<NewsGroup, Long> {
    List<NewsGroup> findByActiveIsTrue();

    Optional<NewsGroup> findByName(String name);
}

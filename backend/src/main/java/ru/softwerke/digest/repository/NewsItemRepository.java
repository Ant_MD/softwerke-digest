package ru.softwerke.digest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.softwerke.digest.entity.NewsItem;

public interface NewsItemRepository extends JpaRepository<NewsItem, Long> {
}

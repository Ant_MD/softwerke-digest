package ru.softwerke.digest.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.softwerke.digest.entity.Mail;

public interface MailRepository extends JpaRepository<Mail, Long> {
}
